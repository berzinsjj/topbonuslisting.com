<?php

// Wordpress settings
add_theme_support('menus');

// Add field for SVG logo
function my_register_additional_customizer_settings( $wp_customize ) {
    $wp_customize->add_setting(
        'my_company_logo',
        array(
            'default' => '',
            'type' => 'option', // you can also use 'theme_mod'
            'capability' => 'edit_theme_options'
        )
    );

    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'my_company_logo',
        array(
            'label'      => __( 'Logo', 'textdomain' ),
            'settings'   => 'my_company_logo',
            'priority'   => 1,
            'section'    => 'title_tagline',
            'type'       => 'text',
        )
    ) );
}
add_action( 'customize_register', 'my_register_additional_customizer_settings' );

// Load main JS file
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script('custom.min.js', get_template_directory_uri() . '/scripts.min.js', array('jquery'));
});

// Allow to upload SVG
function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');

// Remove "JQMIGRATE: Migrate is installed, version 1.4.1" notification from console
add_action('wp_default_scripts', function ($scripts) {
    if (!empty($scripts->registered['jquery'])) {
        $scripts->registered['jquery']->deps = array_diff($scripts->registered['jquery']->deps, array('jquery-migrate'));
    }
});

// Hide admin bar
show_admin_bar(false);

// Custom icons nav menu
add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);
function my_wp_nav_menu_objects($items, $args)
{
    // loop
    foreach ($items as &$item) {
        // vars
        $icon = get_field('icon', $item);
        // append icon
        if ($icon) {
            $item->title .= $icon;
        }
    }
    // return
    return $items;
}

?>

<?php function companyCards($company, $page)
{ ?>
    <div class="casino-boxes" data-best-rated="<?php echo get_field('rating', $company->ID); ?>"
         data-top-bonus="<?php echo get_field('bonus_percent', $company->ID); ?>"
         data-max-bonus="<?php echo get_field('bonus_max', $company->ID); ?>">
        <div class="casino-kaksis">
            <div class="casino-box <?php echo $page === 'single-post-kakina' ? 'single-casino' : ''; ?>">
                <div class="logo-rating-name">
                    <div class="logo" style="background-color: <?php echo get_field('logo_color', $company->ID); ?>;">
                        <a href="<?php echo get_field('affialite_link', $company->ID); ?>" target="_blank"> <img src="<?php echo get_field('logo', $company->ID); ?>"></a>
                    </div>
                    <div class="stars">
                        <?php if (get_field('rating', $company->ID) == 0) : ?>
                            <div class="stars-container">
                                <div class="star">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
   c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                            </div>
                        <?php elseif (get_field('rating', $company->ID) == 1) : ?>
                            <div class="stars-container">
                                <div class="star full">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                            </div>
                        <?php elseif (get_field('rating', $company->ID) == 2) : ?>
                            <div class="stars-container">
                                <div class="star full">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star full">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                            </div>
                        <?php elseif (get_field('rating', $company->ID) == 3) : ?>
                            <div class="stars-container">
                                <div class="star full">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star full">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star full">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                            </div>
                        <?php elseif (get_field('rating', $company->ID) == 4) : ?>
                            <div class="stars-container">
                                <div class="star full">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star full">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star full">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star full">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                            </div>
                        <?php elseif (get_field('rating', $company->ID) == 5) : ?>
                            <div class="stars-container">
                                <div class="star full">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star full">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star full">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star full">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                                <div class="star full">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 28.391 27.055" style="enable-background:new 0 0 28.391 27.055;"
                                         xml:space="preserve"><radialGradient id="SVGID_1_" cx="4.8504" cy="-0.2866"
                                                                              r="29.645"
                                                                              gradientTransform="matrix(1 0 0 -1 0 27.1465)"
                                                                              gradientUnits="userSpaceOnUse">
                                            <stop offset="0.3548" style="stop-color:#FFC200"/>
                                            <stop offset="0.5084" style="stop-color:#FFBD07"/>
                                            <stop offset="0.7249" style="stop-color:#FFAF19"/>
                                            <stop offset="0.9777" style="stop-color:#FF9737"/>
                                            <stop offset="1" style="stop-color:#FF953A"/>
                                        </radialGradient>
                                        <path class="st0" d="M23.143,26.1c0.1,0.9-0.4,1.2-1.2,0.8l-6.4-3.2c-0.8-0.4-2-0.4-2.8,0l-6.4,3.2c-0.8,0.4-1.3,0-1.2-0.9l1.1-7.1
        c0.1-0.9-0.3-2.1-0.9-2.7l-5-5.1c-0.6-0.6-0.4-1.2,0.5-1.4l7.1-1.2c0.9-0.1,1.9-0.9,2.3-1.6l3.3-6.3c0.4-0.8,1.1-0.8,1.4,0l3.3,6.4
        c0.4,0.8,1.4,1.5,2.3,1.7l7.1,1.2c0.8,0.1,1,0.8,0.4,1.4l-5,5.1c-0.6,0.6-1,1.8-0.9,2.7L23.143,26.1z"/></svg>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="name">
                        <a href="<?php echo get_field('affialite_link', $company->ID); ?>" target="_blank"><p><?php echo $company->post_title; ?></p></a>
                    </div>
                </div>
                <div class="bonus-terms">
                    <div class="bonus">
                        <a href="<?php echo get_field('affialite_link', $company->ID); ?>" target="_blank"><?php echo get_field('bonus', $company->ID); ?></a>
                    </div>
                    <div class="second-bonus">
                        <p><?php echo get_field('second_bonus', $company->ID); ?></p>
                    </div>
                    <div class="terms-apply">
                        <a href="<?php echo get_field('terms_link', $company->ID); ?>" target="_blank">BeGambleAware. T&C's apply. 18+</a>
                    </div>
                </div>
                <div class="button-code">
                    <div class="button">
                        <a href="<?php echo get_field('affialite_link', $company->ID); ?>" target="_blank">Join Now</a>
                    </div>
                    <?php if (!empty(get_field('bonus_code', $company->ID))) : ?>
                        <div class="code">
                            <p><?php echo get_field('bonus_code', $company->ID); ?></p>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php if (!empty(get_field('terms_info', $company->ID))) : ?>
            <div class="terms-info">
                <p><?php echo get_field('terms_info', $company->ID); ?></p>
            </div>
        <?php endif; ?>
    </div>
<?php } ?>


