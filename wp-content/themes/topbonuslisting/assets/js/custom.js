jQuery(function ($) {
    jQuery(document).ready(function () {

        // Mobile menu
        $(".hamburger").click(function () {
            $(this).toggleClass("is-active");
            $(".menu-main-menu-container").toggleClass("is-active");
        });

        // Scroll to top
        $(".back-to-top").click(function () {
            $("html, body").animate({scrollTop: 0}, 500);
            return false;
        });

        // Sort rating best rated
        $(".sort-best").click(function (e) {
            // e uztaisa lai neizpilditu A hrefu
            e.preventDefault();
            $(".sort-best, .sort-top, .sort-max").removeClass('sort-active');
            $(this).addClass('sort-active');
            var result = $('.casino-boxes').sort(function (a, b) {
                var contentA =parseInt( $(a).data('best-rated'));
                var contentB =parseInt( $(b).data('best-rated'));
                return (contentA > contentB) ? -1 : (contentA < contentB) ? 1 : 0;
            });
            $('.all-casinos').html(result);
        });

        // Sort rating top bonus %
        $(".sort-top").click(function (e) {
            // e uztaisa lai neizpilditu A hrefu
            e.preventDefault();
            $(".sort-best, .sort-top, .sort-max").removeClass('sort-active');
            $(this).addClass('sort-active');
            var result = $('.casino-boxes').sort(function (a, b) {
                var contentA =parseInt( $(a).data('top-bonus'));
                var contentB =parseInt( $(b).data('top-bonus'));
                return (contentA > contentB) ? -1 : (contentA < contentB) ? 1 : 0;
            });
            $('.all-casinos').html(result);
        });

        // Sort rating mac bonus
        $(".sort-max").click(function (e) {
            // e uztaisa lai neizpilditu A hrefu
            e.preventDefault();
            $(".sort-best, .sort-top, .sort-max").removeClass('sort-active');
            $(this).addClass('sort-active');
            var result = $('.casino-boxes').sort(function (a, b) {
                var contentA =parseInt( $(a).data('max-bonus'));
                var contentB =parseInt( $(b).data('max-bonus'));
                return (contentA > contentB) ? -1 : (contentA < contentB) ? 1 : 0;
            });
            $('.all-casinos').html(result);
        });

    });
});
