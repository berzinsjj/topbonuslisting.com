<?php /* Template Name: Post */ ?>
<?php get_header(); ?>
    <div class="page-container">
        <div class="page-content">
            <div class="page-name-icons">
                <div class="page-name">
                    <?php $page = get_post(get_the_ID()); ?>
                    <p><?php echo $page->page_text; ?></p>
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-double-right" class="svg-inline--fa fa-angle-double-right fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34zm192-34l-136-136c-9.4-9.4-24.6-9.4-33.9 0l-22.6 22.6c-9.4 9.4-9.4 24.6 0 33.9l96.4 96.4-96.4 96.4c-9.4 9.4-9.4 24.6 0 33.9l22.6 22.6c9.4 9.4 24.6 9.4 33.9 0l136-136c9.4-9.2 9.4-24.4 0-33.8z"></path></svg>
                    <span><?php echo $page->post_title; ?></span>
                </div>
            </div>
            <div class="site-name">
                <h1><?php echo $page->post_title; ?></h1>
                <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="sort-down" class="svg-inline--fa fa-sort-down fa-w-10" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path fill="currentColor" d="M41 288h238c21.4 0 32.1 25.9 17 41L177 448c-9.4 9.4-24.6 9.4-33.9 0L24 329c-15.1-15.1-4.4-41 17-41z"></path></svg>
            </div>
            <?php if (!empty(get_field('featured_top'))) : ?>
                <div class="casino-container single-post-casino">
                    <div class="all-casinos">
                        <?php $featured_top = get_field('featured_top'); ?>
                        <?php foreach ($featured_top as $company) : ?>
                            <?php companyCards($company, 'single-post-kakina'); ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (!empty(get_field('paragraph'))) : ?>
                <div class="page-info-paragraph single-post-paragraph">
                    <?php echo nl2br($page->paragraph); ?>
                </div>
            <?php endif; ?>
            <?php if (!empty(get_field('featured_bot'))) : ?>
                <div class="casino-container single-post-casino">
                    <div class="all-casinos">
                        <?php $featured_bot = get_field('featured_bot'); ?>
                        <?php foreach ($featured_bot as $company) : ?>
                            <?php companyCards($company, 'single-post-kakina'); ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php get_footer(); ?>