<footer>
    <?php $footer = get_field('footer', 9); ?>
    <div class="footer-menu-background">
        <div class="footer-icons-first">
            <?php foreach ($footer['footer_icon_first'] as $icon) : ?>
                <div class="footer-icon-first">
                    <a href="<?php echo $icon['svg_link_first']['url']; ?>" target="_blank">
                        <?php echo $icon['svg_first']; ?>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="footer-menus">
            <div class="footer-menu">
                <?php echo wp_nav_menu(array('menu' => 'Footer menu')); ?>
            </div>
            <div class="footer-text">
                <p><?php echo $footer['footer_text']; ?></p>
            </div>
            <div class="footer-icons">
                <?php foreach ($footer['footer_icon'] as $icon) : ?>
                    <div class="footer-icon">
                        <a href="<?php echo $icon['svg_link']['url']; ?>" target="_blank">
                            <?php echo $icon['svg']; ?>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="copyright">
                <p><?php echo $footer['copyright']; ?></p>
            </div>
        </div>
    </div>
</footer>
<div class="back-to-top">
    <a href="/"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-up" class="svg-inline--fa fa-chevron-up fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M240.971 130.524l194.343 194.343c9.373 9.373 9.373 24.569 0 33.941l-22.667 22.667c-9.357 9.357-24.522 9.375-33.901.04L224 227.495 69.255 381.516c-9.379 9.335-24.544 9.317-33.901-.04l-22.667-22.667c-9.373-9.373-9.373-24.569 0-33.941L207.03 130.525c9.372-9.373 24.568-9.373 33.941-.001z"></path></svg></a>
</div>
<?php wp_footer(); ?>
</body>
</html>